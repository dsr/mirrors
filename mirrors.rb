# Insert your mirror directory here
mirror_dir = "/Users/dan/Projects/mirrors"

Dir.chdir(mirror_dir)

Dir["*"].each do |repo|
  Dir.chdir("#{mirror_dir}/#{repo}") 
  IO.popen("git fetch && git push --mirror github") do |pipe|
    p "Fetching and pushing #{repo}."  
    pipe.each do |line|
      p line
    end
  end
end